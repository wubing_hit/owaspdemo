﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Webserver
{
    public class HttpRoute
    {
        public HttpMethod Method;

        public string Path;

        public Func<HttpRequest, HttpResponse> Handler;

        public HttpRoute(HttpMethod method, string path, Func<HttpRequest, HttpResponse> handler)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (handler == null) throw new ArgumentNullException(nameof(handler));

            Method = method;

            Path = path.ToLower();
            if (!Path.StartsWith("/")) Path = "/" + Path;
            if (!Path.EndsWith("/")) Path = Path + "/";

            Handler = handler;
        }
    }
    public class HttpRouteManager
    {
        private List<HttpRoute> _Routes;

        public HttpRouteManager()
        {
            _Routes = new List<HttpRoute>();
        }

        public void Add(HttpMethod method, string path, Func<HttpRequest, HttpResponse> handler)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));
            if (handler == null) throw new ArgumentNullException(nameof(handler));

            HttpRoute r = new HttpRoute(method, path, handler);
            Add(r);
        }

        private void Add(HttpRoute route)
        {
            if (route == null) throw new ArgumentNullException(nameof(route));

            route.Path = route.Path.ToLower();
            if (!route.Path.StartsWith("/")) route.Path = "/" + route.Path;
            if (!route.Path.EndsWith("/")) route.Path = route.Path + "/";

            if (Exists(route.Method, route.Path))
            {
                return;
            }

            _Routes.Add(route);
        }

        public bool Exists(HttpMethod method, string path)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));

            path = path.ToLower();
            if (!path.StartsWith("/")) path = "/" + path;
            if (!path.EndsWith("/")) path = path + "/";

            HttpRoute curr = _Routes.FirstOrDefault(i => i.Method == method && i.Path == path);
            if (curr == null || curr == default(HttpRoute))
            {
                return false;
            }

            return true;
        }

        public Func<HttpRequest, HttpResponse> Match(HttpMethod method, string path)
        {
            if (String.IsNullOrEmpty(path)) throw new ArgumentNullException(nameof(path));

            path = path.ToLower();
            if (!path.StartsWith("/")) path = "/" + path;
            if (!path.EndsWith("/")) path = path + "/";

            HttpRoute curr = _Routes.FirstOrDefault(i => i.Method == method && i.Path == path);
            if (curr == null || curr == default(HttpRoute))
            {
                return null;
            }
            else
            {
                return curr.Handler;
            }
        }
    }
}
