﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.Runtime.Serialization;
using System.IO;

namespace Webserver
{
    public enum HttpMethod
    {
        [EnumMember(Value = "GET")]
        GET,
        [EnumMember(Value = "HEAD")]
        HEAD,
        [EnumMember(Value = "PUT")]
        PUT,
        [EnumMember(Value = "POST")]
        POST,
        [EnumMember(Value = "DELETE")]
        DELETE,
        [EnumMember(Value = "PATCH")]
        PATCH,
        [EnumMember(Value = "CONNECT")]
        CONNECT,
        [EnumMember(Value = "OPTIONS")]
        OPTIONS,
        [EnumMember(Value = "TRACE")]
        TRACE
    }

    
    public class Server
    {
        public HttpRouteManager StaticRoutes;

        private HttpListener httpListener;
        private List<string> listenerHostnames;
        private int listenerPort;
        private bool listenerSsl = false;
        private int StreamReadBufferSize = 65536;

        private Func<HttpRequest, HttpResponse> _DefaultRoute;

        public Server(string hostname, int port, Func<HttpRequest, HttpResponse> defaultRequestHandler)
        {
            if (String.IsNullOrEmpty(hostname)) hostname = "*";
            if (port < 1) throw new ArgumentOutOfRangeException(nameof(port));
            if (defaultRequestHandler == null) throw new ArgumentNullException(nameof(defaultRequestHandler));

            httpListener = new HttpListener();

            listenerHostnames = new List<string>();
            listenerHostnames.Add(hostname);
            listenerPort = port;
            _DefaultRoute = defaultRequestHandler;

            InitializeRouteManagers();
        }

        public void Start()
        {
            foreach (string curr in listenerHostnames)
            {
                string prefix = null;
                if (listenerSsl) prefix = "https://" + curr + ":" + listenerPort + "/";
                else prefix = "http://" + curr + ":" + listenerPort + "/";
                httpListener.Prefixes.Add(prefix);
            }

            httpListener.Start();

            {
                Console.WriteLine("Started on:");
                foreach (var prefix in httpListener.Prefixes)
                {
                    Console.WriteLine("  {0}", prefix);
                }
            }

            while (httpListener.IsListening)
            {
                HttpListenerContext httpListenerContext = httpListener.GetContext();

                HttpRequest httpRequest = new HttpRequest(httpListenerContext);
                HttpResponse httpResponse;

                Func<HttpRequest, HttpResponse> handler = null;

                handler = StaticRoutes.Match(httpRequest.Method, httpRequest.RawUrlWithoutQuery);
                if (handler != null)
                {
                    httpResponse = handler(httpRequest);
                }
                else
                {
                    httpResponse = _DefaultRoute(httpRequest);
                }

                SendResponse(httpListenerContext, httpRequest, httpResponse);
            }
        }

        private void SendResponse(
            HttpListenerContext context,
            HttpRequest req,
            HttpResponse resp)
        {
            long responseLength = 0;
            HttpListenerResponse response = null;

            try
            {
                #region Status-Code-and-Description

                response = context.Response;
                response.StatusCode = resp.StatusCode;

                switch (resp.StatusCode)
                {
                    case 200:
                        response.StatusDescription = "OK";
                        break;

                    case 201:
                        response.StatusDescription = "Created";
                        break;

                    case 301:
                        response.StatusDescription = "Moved Permanently";
                        break;

                    case 302:
                        response.StatusDescription = "Moved Temporarily";
                        break;

                    case 304:
                        response.StatusDescription = "Not Modified";
                        break;

                    case 400:
                        response.StatusDescription = "Bad Request";
                        break;

                    case 401:
                        response.StatusDescription = "Unauthorized";
                        break;

                    case 403:
                        response.StatusDescription = "Forbidden";
                        break;

                    case 404:
                        response.StatusDescription = "Not Found";
                        break;

                    case 405:
                        response.StatusDescription = "Method Not Allowed";
                        break;

                    case 429:
                        response.StatusDescription = "Too Many Requests";
                        break;

                    case 500:
                        response.StatusDescription = "Internal Server Error";
                        break;

                    case 501:
                        response.StatusDescription = "Not Implemented";
                        break;

                    case 503:
                        response.StatusDescription = "Service Unavailable";
                        break;

                    default:
                        response.StatusDescription = "Unknown Status";
                        break;
                }

                #endregion

                #region Response-Headers

                response.AddHeader("Access-Control-Allow-Origin", "*");
                //response.ContentType = req.ContentType;

                if (resp.Headers != null && resp.Headers.Count > 0)
                {
                    foreach (KeyValuePair<string, string> curr in resp.Headers)
                    {
                        response.AddHeader(curr.Key, curr.Value);
                    }
                }

                #endregion

                #region Handle-HEAD-Request

                if (req.Method == HttpMethod.HEAD)
                {
                    resp.Data = null;
                    resp.DataStream = null;
                }

                #endregion

                #region Send-Response

                Stream output = response.OutputStream;
                response.ContentLength64 = resp.ContentLength;

                try
                {
                    if (resp.Data != null && resp.Data.Length > 0)
                    {
                        responseLength = resp.Data.Length;
                        response.ContentLength64 = responseLength;
                        output.Write(resp.Data, 0, (int)responseLength);
                    }
                    else if (resp.DataStream != null && resp.ContentLength > 0)
                    {
                        responseLength = resp.ContentLength;
                        response.ContentLength64 = resp.ContentLength;

                        long bytesRemaining = resp.ContentLength;

                        while (bytesRemaining > 0)
                        {
                            int bytesRead = 0;
                            byte[] buffer = new byte[StreamReadBufferSize];

                            if (bytesRemaining >= StreamReadBufferSize) bytesRead = resp.DataStream.Read(buffer, 0, StreamReadBufferSize);
                            else bytesRead = resp.DataStream.Read(buffer, 0, (int)bytesRemaining);

                            output.Write(buffer, 0, bytesRead);
                            bytesRemaining -= bytesRead;
                        }

                        resp.DataStream.Close();
                        resp.DataStream.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Console.WriteLine("Outer exception");
                    // Console.WriteLine(WatsonCommon.SerializeJson(eInner)); 
                }
                finally
                {
                    output.Flush();
                    output.Close();

                    if (response != null) response.Close();
                }

                #endregion

                return;
            }
            catch (Exception)
            {
                // Console.WriteLine("Outer exception");
                // Console.WriteLine(WatsonCommon.SerializeJson(eOuter));
                return;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }
        }

        private void InitializeRouteManagers()
        {
            StaticRoutes = new HttpRouteManager();
        }


    }
}
