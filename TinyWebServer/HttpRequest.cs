﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Webserver
{
    public class HttpRequest
    {
        public string FullUrl;
        public string RawUrlWithoutQuery;
        public HttpMethod Method;
        public DateTime TimestampUtc;
        public string SourceIp;
        public int SourcePort;
        public string DestIp;
        public int DestPort;
        public string ContentType;
        public Dictionary<string, string> Headers;
        public string BodyData = null;
        public List<string> RawUrlEntries;
        public Dictionary<string, string> QuerystringEntries;
        public string Querystring;
        public bool HasEntityBody;

        public HttpListenerContext httpListenerContext;
        public HttpRequest(HttpListenerContext httpListenerContext)
        {
            this.httpListenerContext = httpListenerContext;

            FullUrl = String.Copy(httpListenerContext.Request.Url.ToString().Trim());
            RawUrlWithoutQuery = String.Copy(httpListenerContext.Request.RawUrl.ToString().Trim());
            Method = (HttpMethod)Enum.Parse(typeof(HttpMethod), httpListenerContext.Request.HttpMethod, true);
            TimestampUtc = DateTime.Now.ToUniversalTime();
            SourceIp = httpListenerContext.Request.RemoteEndPoint.Address.ToString();
            SourcePort = httpListenerContext.Request.RemoteEndPoint.Port;
            DestIp = httpListenerContext.Request.LocalEndPoint.Address.ToString();
            DestPort = httpListenerContext.Request.LocalEndPoint.Port;
            ContentType = httpListenerContext.Request.ContentType;
            HasEntityBody = httpListenerContext.Request.HasEntityBody;

            Headers = new Dictionary<string, string>();
            for (int i = 0; i < httpListenerContext.Request.Headers.Count; i++)
            {
                string key = String.Copy(httpListenerContext.Request.Headers.GetKey(i));
                string val = String.Copy(httpListenerContext.Request.Headers.Get(i));
                Headers = AddToDict(key, val, Headers);
            }

            if (HasEntityBody)
            {
                StreamReader sr = new StreamReader(httpListenerContext.Request.InputStream,
                    httpListenerContext.Request.ContentEncoding);
                BodyData = sr.ReadToEnd();
                sr.Close();
                httpListenerContext.Request.InputStream.Close();
            }

            if (!String.IsNullOrEmpty(RawUrlWithoutQuery))
            {
                #region Parse-Variables

                int position = 0;
                int inQuery = 0;
                string tempString = "";
                string queryString = "";

                int inKey = 0;
                int inVal = 0;
                string tempKey = "";
                string tempVal = "";

                #endregion
                #region Initialize-Variables

                RawUrlEntries = new List<string>();
                QuerystringEntries = new Dictionary<string, string>();

                #endregion

                #region Process-Raw-URL-and-Populate-Raw-URL-Elements

                while (RawUrlWithoutQuery.Contains("//"))
                {
                    RawUrlWithoutQuery = RawUrlWithoutQuery.Replace("//", "/");
                }

                foreach (char c in RawUrlWithoutQuery)
                {
                    if (inQuery == 1)
                    {
                        queryString += c;
                        continue;
                    }

                    if ((position == 0) &&
                        (String.Compare(tempString, "") == 0) &&
                        (c == '/'))
                    {
                        // skip the first slash
                        continue;
                    }

                    if ((c != '/') && (c != '?'))
                    {
                        tempString += c;
                    }

                    if ((c == '/') || (c == '?'))
                    {
                        if (!String.IsNullOrEmpty(tempString))
                        {
                            // add to raw URL entries list
                            RawUrlEntries.Add(tempString);
                        }

                        position++;
                        tempString = "";
                    }

                    if (c == '?')
                    {
                        inQuery = 1;
                    }
                }

                if (!String.IsNullOrEmpty(tempString))
                {
                    // add to raw URL entries list
                    RawUrlEntries.Add(tempString);
                }

                #endregion

                #region Populate-Querystring

                if (queryString.Length > 0) Querystring = queryString;
                else Querystring = null;

                #endregion

                #region Parse-Querystring

                if (!String.IsNullOrEmpty(Querystring))
                {
                    inKey = 1;
                    inVal = 0;
                    position = 0;
                    tempKey = "";
                    tempVal = "";

                    foreach (char c in Querystring)
                    {
                        if (inKey == 1)
                        {
                            if (c == '&')
                            {
                                // key with no value
                                if (!String.IsNullOrEmpty(tempKey))
                                {
                                    inKey = 1;
                                    inVal = 0;

                                    tempKey = WebUtility.UrlDecode(tempKey);
                                    QuerystringEntries = AddToDict(tempKey, null, QuerystringEntries);

                                    tempKey = "";
                                    tempVal = "";
                                    position++;
                                    continue;
                                }
                            }
                            else if (c != '=')
                            {
                                tempKey += c;
                            }
                            else
                            {
                                inKey = 0;
                                inVal = 1;
                                continue;
                            }
                        }

                        if (inVal == 1)
                        {
                            if (c != '&')
                            {
                                tempVal += c;
                            }
                            else
                            {
                                inKey = 1;
                                inVal = 0;

                                tempKey = WebUtility.UrlDecode(tempKey);
                                if (!String.IsNullOrEmpty(tempVal)) tempVal = WebUtility.UrlDecode(tempVal);
                                QuerystringEntries = AddToDict(tempKey, tempVal, QuerystringEntries);

                                tempKey = "";
                                tempVal = "";
                                position++;
                                continue;
                            }
                        }
                    }

                    if (inVal == 0)
                    {
                        // val will be null
                        if (!String.IsNullOrEmpty(tempKey))
                        {
                            tempKey = WebUtility.UrlDecode(tempKey);
                            QuerystringEntries = AddToDict(tempKey, null, QuerystringEntries);
                        }
                    }

                    if (inVal == 1)
                    {
                        if (!String.IsNullOrEmpty(tempKey))
                        {
                            tempKey = WebUtility.UrlDecode(tempKey);
                            if (!String.IsNullOrEmpty(tempVal)) tempVal = WebUtility.UrlDecode(tempVal);
                            QuerystringEntries = AddToDict(tempKey, tempVal, QuerystringEntries);
                        }
                    }
                }

                #endregion
            }

            if (RawUrlWithoutQuery.Contains("?"))
            {
                RawUrlWithoutQuery = RawUrlWithoutQuery.Substring(0, RawUrlWithoutQuery.IndexOf("?"));
            }
        }
        public static byte[] StreamToBytes(Stream input)
        {
            if (input == null) throw new ArgumentNullException(nameof(input));
            if (!input.CanRead) throw new InvalidOperationException("Input stream is not readable");

            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;

                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }

                return ms.ToArray();
            }
        }
        public static Dictionary<string, string> AddToDict(string key, string val, Dictionary<string, string> existing)
        {
            if (String.IsNullOrEmpty(key)) return existing;

            Dictionary<string, string> ret = new Dictionary<string, string>();

            if (existing == null)
            {
                ret.Add(key, val);
                return ret;
            }
            else
            {
                if (existing.ContainsKey(key))
                {
                    if (String.IsNullOrEmpty(val)) return existing;
                    string tempVal = existing[key];
                    tempVal += "," + val;
                    existing.Remove(key);
                    existing.Add(key, tempVal);
                    return existing;
                }
                else
                {
                    existing.Add(key, val);
                    return existing;
                }
            }
        }
    }
}
