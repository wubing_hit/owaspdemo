﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Webserver
{
    public class HttpResponse
    {
        public DateTime TimestampUtc;
        public string ProtocolVersion;
        public string SourceIp;
        public int SourcePort;
        public string DestIp;
        public int DestPort;
        public HttpMethod Method;
        public string RawUrlWithoutQuery;
        public int StatusCode;
        public string StatusDescription;
        public Dictionary<string, string> Headers;
        public string ContentType;
        public long ContentLength;
        public byte[] Data;
        public Stream DataStream;
        public string DataMd5;

        public HttpResponse(HttpRequest req, int status, Dictionary<string, string> headers, string contentType, byte[] data)
        {
            if (req == null) throw new ArgumentNullException(nameof(req));

            SetBaseVariables(req, status, headers, contentType);
            SetStatusDescription();

            DataStream = null;
            Data = data;
            if (Data != null && Data.Length > 0) ContentLength = Data.Length;
        }

        public HttpResponse(HttpRequest req, int status, Dictionary<string, string> headers, string contentType, string data)
        {
            if (req == null) throw new ArgumentNullException(nameof(req));

            SetBaseVariables(req, status, headers, contentType);
            SetStatusDescription();

            DataStream = null;
            Data = null;
            if (!String.IsNullOrEmpty(data)) Data = Encoding.UTF8.GetBytes(data);
            if (Data != null && Data.Length > 0) ContentLength = Data.Length;
        }

        private void SetBaseVariables(HttpRequest req, int status, Dictionary<string, string> headers, string contentType)
        {
            TimestampUtc = req.TimestampUtc;
            SourceIp = req.SourceIp;
            SourcePort = req.SourcePort;
            DestIp = req.DestIp;
            DestPort = req.DestPort;
            Method = req.Method;
            RawUrlWithoutQuery = req.RawUrlWithoutQuery;

            Headers = headers;
            ContentType = contentType;
            if (String.IsNullOrEmpty(ContentType)) ContentType = "application/octet-stream";

            StatusCode = status;
        }

        private void SetStatusDescription()
        {
            switch (StatusCode)
            {
                case 200:
                    StatusDescription = "OK";
                    break;

                case 201:
                    StatusDescription = "Created";
                    break;

                case 204:
                    StatusDescription = "No Content";
                    break;

                case 301:
                    StatusDescription = "Moved Permanently";
                    break;

                case 302:
                    StatusDescription = "Moved Temporarily";
                    break;

                case 304:
                    StatusDescription = "Not Modified";
                    break;

                case 400:
                    StatusDescription = "Bad Request";
                    break;

                case 401:
                    StatusDescription = "Unauthorized";
                    break;

                case 403:
                    StatusDescription = "Forbidden";
                    break;

                case 404:
                    StatusDescription = "Not Found";
                    break;

                case 405:
                    StatusDescription = "Method Not Allowed";
                    break;

                case 429:
                    StatusDescription = "Too Many Requests";
                    break;

                case 500:
                    StatusDescription = "Internal Server Error";
                    break;

                case 501:
                    StatusDescription = "Not Implemented";
                    break;

                case 503:
                    StatusDescription = "Service Unavailable";
                    break;

                default:
                    StatusDescription = "Unknown";
                    return;
            }
        }
    }
}
