﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;
using System.Data.SQLite;
using System.Diagnostics;

namespace OWASPDemoConsole
{
    class A7XSSPhishing2
    {
        private static IDictionary<string, string> GetCookieData(string cookie)
        {
            var cookieDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            if (!cookie.Any())
            {
                return cookieDictionary;
            }

            var values = cookie.TrimEnd(';').Split(';');
            foreach (var parts in values.Select(c => c.Split(new[] { '=' }, 2)))
            {
                var cookieName = parts[0].Trim();
                string cookieValue;

                if (parts.Length == 1)
                {
                    //Cookie attribute
                    cookieValue = string.Empty;
                }
                else
                {
                    cookieValue = parts[1];
                }

                cookieDictionary[cookieName] = cookieValue;
            }

            return cookieDictionary;
        }

        protected static Webserver.HttpResponse HandleLogin(Webserver.HttpRequest req)
        {
            if (!req.HasEntityBody)
            {
                return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(Util.LoadHtml(@"OWASPDemoConsole.HTML.A7.login.html")));
            }
            else
            {
                string name = HttpUtility.ParseQueryString(req.BodyData).Get("name");
                string password = HttpUtility.ParseQueryString(req.BodyData).Get("password");

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql = @"SELECT COUNT(*) FROM Users WHERE Name=@name AND Password=@password";
                    SQLiteCommand command = new SQLiteCommand(sql, con);
                    command.Parameters.Add(new SQLiteParameter("@name", name));
                    command.Parameters.Add(new SQLiteParameter("@password", password));
                    long count = (long)command.ExecuteScalar();

                    if (count != 0)
                    {
                        //return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(@"Login successful"));
                        string guid = Guid.NewGuid().ToString();
                        Authentications[guid] = name;

                        Webserver.HttpResponse res = ReturnUserInfo(req, name);

                        res.Headers = new Dictionary<string, string>();
                        res.Headers["Set-Cookie"] = @"Authentication=" + guid;

                        return res;
                    }
                    else
                    {
                        return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(@"Login failed"));
                    }
                }
            }
        }

        static Dictionary<string, string> Authentications = new Dictionary<string, string>();

        public static string GetCurrentUser(Webserver.HttpRequest req)
        {
            if (!req.Headers.ContainsKey("Cookie"))
            {
                return null;
            }

            IDictionary<string, string> cookies = GetCookieData(req.Headers["Cookie"]);

            if (!cookies.ContainsKey("Authentication"))
            {
                return null;
            }

            string auth = cookies["Authentication"];

            if (!Authentications.ContainsKey(auth))
            {
                return null;
            }

            return Authentications[auth];
        }
        public static Webserver.HttpResponse Handle(Webserver.HttpRequest req)
        {
            string user = GetCurrentUser(req);

            if (user == null)
            {
                return HandleLogin(req);
            }

            return ReturnUserInfo(req, user);
        }

        private static Webserver.HttpResponse ReturnUserInfo(Webserver.HttpRequest req, string user)
        {
            StringBuilder sb = new StringBuilder();
            using (HtmlTable.Table table = new HtmlTable.Table(sb, id: "some-id"))
            {
                table.StartHead();
                using (var thead = table.AddRow())
                {
                    thead.AddCell(@"CustomerID");
                    thead.AddCell(@"CompanyName");
                    thead.AddCell(@"ContactName");
                    thead.AddCell(@"ContactTitle");
                    thead.AddCell(@"Address");
                    thead.AddCell(@"City");
                    thead.AddCell(@"Region");
                    thead.AddCell(@"PostalCode");
                    thead.AddCell(@"Country");
                    thead.AddCell(@"Phone");
                    thead.AddCell(@"Fax");
                }
                table.EndHead();
                table.StartBody();

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql =
@"SELECT 
CustomerID, 
CompanyName, 
ContactName, 
ContactTitle,
Address,
City,
Region,
PostalCode,
Country,
Phone,
Fax
FROM Customers";
                    SQLiteCommand command = new SQLiteCommand(sql, con);

                    try
                    {
                        using (SQLiteDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    using (var tr = table.AddRow())
                                    {
                                        tr.AddCell(dr.GetValue(0).ToString());
                                        tr.AddCell(dr.GetValue(1).ToString());
                                        tr.AddCell(dr.GetValue(2).ToString());
                                        tr.AddCell(dr.GetValue(3).ToString());
                                        tr.AddCell(dr.GetValue(4).ToString());
                                        tr.AddCell(dr.GetValue(5).ToString());
                                        tr.AddCell(dr.GetValue(6).ToString());
                                        tr.AddCell(dr.GetValue(7).ToString());
                                        tr.AddCell(dr.GetValue(8).ToString());
                                        tr.AddCell(dr.GetValue(9).ToString());
                                        tr.AddCell(dr.GetValue(10).ToString());
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }

                table.EndBody();
            }

            string html = Util.LoadHtml(@"OWASPDemoConsole.HTML.A7.customers.html");
            html = html.Replace(@"<!--CUSTOMERS-->", sb.ToString());

            return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(html));
            
        }
    }
}
