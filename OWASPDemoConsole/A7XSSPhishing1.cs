﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;
using System.Data.SQLite;
using System.Diagnostics;
using System.Xml;
using System.IO;

namespace OWASPDemoConsole
{
    class A7XSSPhishing1
    {

        public static Webserver.HttpResponse Handle(Webserver.HttpRequest req)
        {
            if (req.HasEntityBody)
            {
                string comment = HttpUtility.ParseQueryString(req.BodyData).Get("comment");

                string user = A7XSSPhishing2.GetCurrentUser(req);
                if (user == null)
                {
                    user = @"匿名用户";
                }

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql = @"INSERT INTO A7Comments (TimeStamp, UserName, Comment) VALUES (@TimeStamp, @UserName,@Comment)";
                    SQLiteCommand command = new SQLiteCommand(sql, con);
                    command.Parameters.Add(new SQLiteParameter("@TimeStamp", String.Format(@"{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now)));
                    command.Parameters.Add(new SQLiteParameter("@UserName", user));
                    command.Parameters.Add(new SQLiteParameter("@Comment", comment));
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }
            }
            
            StringBuilder sb = new StringBuilder();
            using (HtmlTable.Table table = new HtmlTable.Table(sb, id: "some-id"))
            {
                table.StartHead();
                using (var thead = table.AddRow())
                {
                    thead.AddCell(@"时间");
                    thead.AddCell(@"用户");
                    thead.AddCell(@"留言");
                }
                table.EndHead();
                table.StartBody();

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql = @"SELECT TimeStamp, UserName, Comment FROM A7Comments ORDER BY TimeStamp";
                    SQLiteCommand command = new SQLiteCommand(sql, con);

                    try
                    {
                        using (SQLiteDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    using (var tr = table.AddRow())
                                    {
                                        tr.AddCell(dr.GetString(0));
                                        tr.AddCell(dr.GetString(1));
                                        tr.AddCell(dr.GetString(2));
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }

                table.EndBody();
            }

            string html = Util.LoadHtml(@"OWASPDemoConsole.HTML.A7.comments.html");
            html = html.Replace(@"<!--COMMENTS-->", sb.ToString());
            return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(html));
        }
    }
}