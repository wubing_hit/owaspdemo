﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;
using System.Data.SQLite;
using System.Diagnostics;

namespace OWASPDemoConsole
{
    class A1SQLOrderBy1
    {
        public static Webserver.HttpResponse Handle(Webserver.HttpRequest req)
        {
            Uri myUri = new Uri(req.FullUrl);
            string sortKey = HttpUtility.ParseQueryString(myUri.Query).Get("sort");

            if (sortKey == null)
            {
                sortKey = "ProductName";
            }

            StringBuilder sb = new StringBuilder();
            using (HtmlTable.Table table = new HtmlTable.Table(sb, id: "some-id"))
            {
                table.StartHead();
                using (var thead = table.AddRow())
                {
                    thead.AddCell(@"<a href=""/A1SQLOrderBy1?sort=ProductName"">品名</a>");
                    thead.AddCell(@"<a href=""/A1SQLOrderBy1?sort=QuantityPerUnit"">容积</a>");
                    thead.AddCell(@"<a href=""/A1SQLOrderBy1?sort=UnitPrice"">单价</a>");
                    thead.AddCell(@"<a href=""/A1SQLOrderBy1?sort=UnitsInStock"">库存量</a>");
                }
                table.EndHead();
                table.StartBody();

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql = @"SELECT ProductName, QuantityPerUnit, UnitPrice, UnitsInStock FROM Products ORDER BY " + sortKey;
                    SQLiteCommand command = new SQLiteCommand(sql, con);

                    try
                    {
                        using (SQLiteDataReader dr = command.ExecuteReader())
                        {
                            if (dr.HasRows)
                            {
                                while (dr.Read())
                                {
                                    using (var tr = table.AddRow())
                                    {
                                        tr.AddCell(dr.GetString(0));
                                        tr.AddCell(dr.GetString(1));
                                        tr.AddCell(dr.GetValue(2).ToString());
                                        tr.AddCell(dr.GetValue(3).ToString());
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.ToString());
                    }
                }

                table.EndBody();
            }

            string html = Util.LoadHtml(@"OWASPDemoConsole.HTML.A1.products.html");
            html = html.Replace(@"<!--PRODUCTS-->", sb.ToString());

            return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(html));
        }
    }
}