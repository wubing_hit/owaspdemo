﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;
using System.Data.SQLite;

namespace OWASPDemoConsole
{
    class A1SQLLogin2
    {
        public static Webserver.HttpResponse Handle(Webserver.HttpRequest req)
        {
            if (!req.HasEntityBody)
            {
                return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(Util.LoadHtml(@"OWASPDemoConsole.HTML.A1.login2.html")));
            }
            else
            {
                string name = HttpUtility.ParseQueryString(req.BodyData).Get("name");
                string password = HttpUtility.ParseQueryString(req.BodyData).Get("password");

                using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
                {
                    con.Open();

                    string sql = @"SELECT COUNT(*) FROM Users WHERE Name=@name AND Password=@password";
                    SQLiteCommand command = new SQLiteCommand(sql, con);
                    command.Parameters.Add(new SQLiteParameter("@name", name));
                    command.Parameters.Add(new SQLiteParameter("@password", password));
                    long count = (long)command.ExecuteScalar();

                    if (count != 0)
                    {
                        return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(@"Login successful"));
                    }
                    else
                    {
                        return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(@"Login failed"));
                    }
                }
            }
        }
    }
}
