﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;
using System.IO;
using System.Reflection;

namespace OWASPDemoConsole
{
    class Program
    {
        private static readonly int defaultPort = 9000;

        private static int GetPort(string[] args)
        {
            int _port = -1;

            if (args.Length == 0)
            {
                return defaultPort;
            }
            if (args.Length > 1)
            {
                Console.WriteLine("Invalid parameters");
                
                return defaultPort;
            }
            else if (args.Length == 1)
            {
                if (int.TryParse(args[0], out _port) && _port > 1 && _port < 65535)
                {
                    return _port;
                }
                else
                {
                    Console.WriteLine("Invalid parameters");
                    return defaultPort;
                }
            }

            return defaultPort;
        }
        static void Main(string[] args)
        {

            Server s = new Server("*", GetPort(args), DefaultRoute);
 
            s.StaticRoutes.Add(HttpMethod.GET, "/", HandleDefault);
            s.StaticRoutes.Add(HttpMethod.GET, "/ResetServer/", ResetServer);

            s.StaticRoutes.Add(HttpMethod.GET, "/A1SQLLogin1/", A1SQLLogin1.Handle);
            s.StaticRoutes.Add(HttpMethod.POST, "/A1SQLLogin1/", A1SQLLogin1.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A1SQLLogin2/", A1SQLLogin2.Handle);
            s.StaticRoutes.Add(HttpMethod.POST, "/A1SQLLogin2/", A1SQLLogin2.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A1SQLOrderBy1/", A1SQLOrderBy1.Handle);
            //s.StaticRoutes.Add(HttpMethod.POST, "/A1SQLOrderBy1/", A1SQLOrderBy1.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A1SQLOrderBy2/", A1SQLOrderBy2.Handle);
            //s.StaticRoutes.Add(HttpMethod.POST, "/A1SQLOrderBy2/", A1SQLOrderBy2.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A4XXE1/", A4XXE1.Handle);
            s.StaticRoutes.Add(HttpMethod.POST, "/A4XXE1/", A4XXE1.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A7XSSPhishing1/", A7XSSPhishing1.Handle);
            s.StaticRoutes.Add(HttpMethod.POST, "/A7XSSPhishing1/", A7XSSPhishing1.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/A7XSSPhishing2/", A7XSSPhishing2.Handle);
            s.StaticRoutes.Add(HttpMethod.POST, "/A7XSSPhishing2/", A7XSSPhishing2.Handle);

            s.StaticRoutes.Add(HttpMethod.GET, "/test/", Test);
            s.StaticRoutes.Add(HttpMethod.POST, "/test/", Test);

            s.Start();
        }

        static Webserver.HttpResponse ResetServer(Webserver.HttpRequest req)
        {
            using (SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db"))
            {
                con.Open();
                SQLiteCommand command = new SQLiteCommand(con);

                command.CommandText = @"DELETE FROM A4Comments WHERE UserName <> 'admin'";
                command.ExecuteNonQuery();

                command.CommandText = @"DELETE FROM A7Comments WHERE UserName <> 'admin'";
                command.ExecuteNonQuery();

                return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(@"重置成功"));
            }
        }

        static Webserver.HttpResponse HandleDefault(Webserver.HttpRequest req)
        {
            return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(Util.LoadHtml(@"OWASPDemoConsole.HTML.default.html")));
        }

        static Webserver.HttpResponse DefaultRoute(Webserver.HttpRequest req)
        {
            return new Webserver.HttpResponse(req, 200, null, "text/plain", "Unknown page");
        }

        static Webserver.HttpResponse Test(Webserver.HttpRequest req)
        {
            if (req.HasEntityBody)
            {
                string name = HttpUtility.ParseQueryString(req.BodyData).Get("textfield");
                string password = HttpUtility.ParseQueryString(req.BodyData).Get("textarea");

                return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes("[" + req.BodyData + "]"));
            }
            else
            {
                return new Webserver.HttpResponse(req, 200, null, "text/plain", Encoding.UTF8.GetBytes(Util.LoadHtml(@"OWASPDemoConsole.HTML.test.html")));
            }

        }
    }

    
}
