﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Webserver;

namespace HackerDaemon
{
    class Program
    {
        private static readonly int defaultPort = 9090;

        private static int GetPort(string[] args)
        {
            int _port = -1;

            if (args.Length == 0)
            {
                return defaultPort;
            }
            if (args.Length > 1)
            {
                Console.WriteLine("Invalid parameters");

                return defaultPort;
            }
            else if (args.Length == 1)
            {
                if (int.TryParse(args[0], out _port) && _port > 1 && _port < 65535)
                {
                    return _port;
                }
                else
                {
                    Console.WriteLine("Invalid parameters");
                    return defaultPort;
                }
            }

            return defaultPort;
        }

        static void Main(string[] args)
        {
            //SQLiteConnection con = new SQLiteConnection(@"Data Source=" + AppDomain.CurrentDomain.BaseDirectory + "DemoDB.db");
            //con.Open();

            Server s = new Server("127.0.0.1", GetPort(args), DefaultRoute);
            s.Start();
        }

        static Webserver.HttpResponse DefaultRoute(Webserver.HttpRequest req)
        {
            Console.WriteLine(req.FullUrl);
            return new Webserver.HttpResponse(req, 200, null, "text/plain", "");
        }

    }


}
